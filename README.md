# 目标管理

## 简介

本篇Codelab基于ArkTS实现一个目标管理器，介绍页面级变量的状态管理。效果图如下：

![](screenshots/device/TargetManagement.gif)

## 相关概念

- 自定义弹窗: 通过CustomDialogController类显示自定义弹窗。

- List列表: 列表包含一系列相同宽度的列表项。

## 相关权限

不涉及

## 使用说明

1. 打开应用首页，点击主页添加子目标按钮，展示添加子目标弹窗，点击取消按钮关闭弹窗，或者确定按钮保存数据。
2. 打开应用首页，点击列表项展开卡片，调节进度，点击确定按钮，卡片收起，保存调节的进度值，进度值为100%时，卡片置灰。
3. 打开应用首页，点击编辑按钮，展示多选框，勾选需要删除的卡片，点击删除按钮，数据被删除。

## 约束与限制

1. 本示例仅支持标准系统上运行，支持设备：华为手机。
2. HarmonyOS系统：HarmonyOS NEXT Developer Beta1及以上。
3. DevEco Studio版本：DevEco Studio NEXT Developer Beta1及以上。
4. HarmonyOS SDK版本：HarmonyOS NEXT Developer Beta1 SDK及以上。
